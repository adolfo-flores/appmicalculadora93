package com.example.appmicalculadora93

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat

class OperacionesActivity : AppCompatActivity() {

    private lateinit var txtUsuario : TextView
    private lateinit var txtNum1: EditText
    private lateinit var txtNum2 : EditText
    private lateinit var txtResultado : TextView

    private lateinit var btnSumar : Button
    private lateinit var btnRestar : Button
    private lateinit var btnMultiplicar : Button
    private lateinit var btnDividir : Button

    private lateinit var btnLimpiar : Button
    private lateinit var btnCerrar : Button
    private lateinit var operacion : Operaciones



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(R.layout.activity_operaciones)
        iniciarComponentes()
        eventoClic()

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }
    }
    public fun iniciarComponentes(){
        txtUsuario = findViewById(R.id.txtUsuario)
        txtNum1 = findViewById(R.id.txtNum1)
        txtNum2 = findViewById(R.id.txtNum2)
        txtResultado = findViewById(R.id.txtResultado)

        btnSumar = findViewById(R.id.btnSumar)
        btnRestar = findViewById(R.id.btnRestar)
        btnMultiplicar = findViewById(R.id.btnMultiplicar)
        btnDividir = findViewById(R.id.btnDividir)

        btnLimpiar = findViewById(R.id.btnLimpiar)
        btnCerrar = findViewById(R.id.btnCerrar)

        val bundle : Bundle? = intent.extras
        txtUsuario.text = bundle?.getString("usuario")
    }

    fun obtenerNumeros(): Pair<Float, Float> {
        val num1 = txtNum1.text.toString().toFloatOrNull() ?: 0f
        val num2 = txtNum2.text.toString().toFloatOrNull() ?: 0f
        return Pair(num1, num2)
    }

    public fun validar() : Boolean{
        if(txtNum1.text.toString().contentEquals("")
            || txtNum2.text.toString().contentEquals("")){
            Toast.makeText(this,"Falto informacion",
                Toast.LENGTH_SHORT).show()
            return false

        }
        else{
            return true
        }
    }

    public fun eventoClic(){
        btnSumar.setOnClickListener(View.OnClickListener {
            if(validar()==true){
                val (num1,num2) = obtenerNumeros()
                operacion = Operaciones(num1,num2)
                val resultado = operacion.sumar()
                txtResultado.text = "$resultado"
            }

        })
        btnRestar.setOnClickListener(View.OnClickListener {
            if(validar()==true){
                val (num1,num2) = obtenerNumeros()
                operacion = Operaciones(num1,num2)
                val resultado = operacion.resta()
                txtResultado.text = "$resultado"
            }
        })
        btnMultiplicar.setOnClickListener(View.OnClickListener {
            if(validar()==true){
                val (num1,num2) = obtenerNumeros()
                operacion = Operaciones(num1,num2)
                val resultado = operacion.multiplicar()
                txtResultado.text = "$resultado"
            }
        })
        btnDividir.setOnClickListener(View.OnClickListener {
            if(validar()==true){
                val (num1,num2) = obtenerNumeros()
                if(num2 == 0f){
                    txtResultado.text = "Error"
                }
                else{
                    operacion = Operaciones(num1,num2)
                    val resultado = operacion.dividir()
                    txtResultado.text = "$resultado"
                }
            }
        })
        btnCerrar.setOnClickListener(View.OnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Calculadora")
            builder.setMessage("¿Deseas cerrar la aplicacion?")
            builder.setPositiveButton(android.R.string.yes){
                dialog, whitch -> this.finish()
            }
            builder.setNegativeButton(android.R.string.no){
                dialog , whitch ->
            }
            builder.show()
        })

        btnLimpiar.setOnClickListener(View.OnClickListener {
            txtNum1.setText("")
            txtNum2.setText("")
            txtResultado.text = ""
        })
    }
}